import type { VideoDetails } from "../../lib/graphql/__generated__/VideoDetails";
import PouchDB from 'pouchdb-react-native'
import { dedentBlockStringValue } from 'graphql/language/blockString';
import RNBackgroundDownloader from 'react-native-background-downloader';
import RNFS from 'react-native-fs';

PouchDB.plugin(require('pouchdb-adapter-asyncstorage').default)
export const videoDB = new PouchDB('vid3', {adapter: 'asyncstorage'});


export type CompositeVideoItem = {
  lastWatchedDate: ?Date,
  firstWatchedSinceAuthorization: ?Date,
  authorizedDate: Date,
  partiallyWatched: boolean,
  timeLeftToWatchString: ?Number,
  localVideoUri: ?String,
  localPosterUri: ?String,
  isDownloading: boolean,
  downloaded: boolean,
  downloadPercent: ?Number,
} | VideoDetais;

const MapVideos = ({ doc }) => {
  const { typename } = doc;
  return Object.assign(doc, { __typename: typename });
};

export const GetAllVideos = async (): array<CompositeVideoItem> => {
  try {
    const result = await videoDB.allDocs({include_docs: true});
    const mappedValues = result.rows.map(MapVideos);
    return mappedValues;
  } catch(err) {
    console.log("issue getting videos from db", err);
    return [];
  }
}

// This should be called right before the video is about to be played
// this will control the state in the db.
export const GetForPlayback = async (video: VideoDetails) => {
  try {
    var vid = await videoDB.get(video.id);
    videoDB.put(Object.assign(
      vid,
      {
        partiallyWatched: true,
        lastWatchedDate: new Date(),
        firstWatchedSinceAuthorization: vid.firstWatchedSinceAuthorization ? vid.firstWatchedSinceAuthorization : new Date(),
      }
    ));
    return Object.assign(video, vid);
  } catch (err) {
    // we don't have the video so we just return the one passed in;
    return video;
  }
}

const CleanVideoItem = ({__typename, ...rest}: VideoDetails) =>
  Object.assign({_id: rest.id, typename: __typename}, rest);

const DownloadVideo = (vidProp) => {
  const localVideoUri = `${RNBackgroundDownloader.directories.documents}/${vidProp.id}.mp4`;
  const localPosterUri = `${RNBackgroundDownloader.directories.documents}/${vidProp.id}.jpeg`;

  console.log('Download video:', vidProp);
  RNBackgroundDownloader.download({
    id: vidProp.id,
    url: vidProp.hd,
    destination: localVideoUri
  }).begin((expectedBytes) => {
    console.log(`Going to download ${expectedBytes} bytes!`);
  }).progress((percent) => {
    UpdatePercentage(vidProp, percent * 100);
    console.log(`Downloaded: ${percent * 100}%`);
  }).done(() => {
    DownloadPoster(vidProp, localPosterUri);

    PutOrUpdateVideoForDownload(vidProp, localVideoUri, localPosterUri);
  }).error((error) => {
    console.log('Download canceled due to error: ', error);
  });
}

// Checks the db to see if the video needs to be downloaded vs reauth
export const CheckDB = async (vidProp) => {
  const localVideoUri = `${RNBackgroundDownloader.directories.documents}/${vidProp.id}.mp4`;
  try {
    const result = await videoDB.get(vidProp.id);
    const existsInFS = await RNFS.exists(localVideoUri)
    if (existsInFS) {
      console.log('Exists in fs...', existsInFS);
      PutOrUpdateVideoForDownload(vidProp, result.localVideoUri, result.localPosterUri);
    } else {
      console.log('Needs to be downloaded...');
      DownloadVideo(vidProp)
    }
  } catch (err) {
    DownloadVideo(vidProp);
  }
}

const DownloadPoster = (vidProp, localPosterUri) => {
  console.log('Downloading Poster...', localPosterUri);
  RNBackgroundDownloader.download({
    id: vidProp.id,
    url: vidProp.largeImage,
    destination: localPosterUri,
  }).done(() => {
    console.log('Downloaded Poster!');
  });
}

export const SubscribeToAll = () => {
   const changes = db.changes({
      since: 'now',
      live: true,
      include_docs: true
    }).on('change', function(change) {
      // handle change
    }).on('complete', function(info) {
      // changes() was canceled
    }).on('error', function (err) {
      console.log(err);
    });
    return changes;
}

export const UpdatePercentage = (vidProp, downloadPercent) => {
  const video = CleanVideoItem(vidProp);
  videoDB.get(video._id)
    .then(vid => {
      // silently update the authorization date
      videoDB.put(Object.assign(vid, video, { downloadPercent }))
      .then(() => {})
      .catch(() => {});
    })
    .catch(() => {});
}

// Take a video object and put some salt and pepper around the
// details of what should be stored ( specifically the type above)
export const PutOrUpdateVideoForDownload = (vidProp: VideoDetails, localVideoUri: string, localPosterUri: string) => {
  const video = CleanVideoItem(vidProp);
  videoDB.get(video._id)
    .then(vid => {
      // silently update the authorization date
      videoDB.put(Object.assign(vid, video, {
        authorizedDate: new Date(),
        partiallyWatched: false,
        firstWatchedSinceAuthorization: null,
        localVideoUri,
        localPosterUri,
      }))
      .then(() => {})
      .catch(() => {});
    })
    .catch(err => {
      videoDB.put(Object.assign(video, {
        authorizedDate: new Date(),
        partiallyWatched: false,
        firstWatchedSinceAuthorization: null,
        localVideoUri,
        localPosterUri,
      }))
      .then((x) => {})
      .catch((y) => {console.log("error inserting", y, video)});
    });
};
