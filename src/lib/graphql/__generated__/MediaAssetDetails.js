

/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: MediaAssetDetails
// ====================================================

export type MediaAssetDetails = {
  __typename: "MediaAsset",
  id: string,
  metadataKey: ?string,
  metadataValue: ?string,
  order: number,
};

/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================