

/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SubAccountDetails
// ====================================================

export type SubAccountDetails = {
  __typename: "SubAccount",
  id: string,
  email: ?string,
  invitePending: boolean,
  profileUrl: ?string,
  role: string,
};

/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================