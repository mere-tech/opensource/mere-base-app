

/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: UserDetails
// ====================================================

export type UserDetails_roles = {
  __typename: "Role",
  id: string,
  name: string,
};

export type UserDetails_subAccounts = {
  __typename: "SubAccount",
  id: string,
  email: ?string,
  invitePending: boolean,
  profileUrl: ?string,
  role: string,
};

export type UserDetails = {
  __typename: "User",
  id: string,
  email: string,
  isSubAccount: boolean,
  roles: Array<UserDetails_roles>,
  subAccounts: Array<UserDetails_subAccounts>,
};

/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================