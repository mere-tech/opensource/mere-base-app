

/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: UserExtendedDetails
// ====================================================

export type UserExtendedDetails_roles = {
  __typename: "Role",
  id: string,
  name: string,
};

export type UserExtendedDetails_subAccounts = {
  __typename: "SubAccount",
  id: string,
  email: ?string,
  invitePending: boolean,
  profileUrl: ?string,
  role: string,
};

export type UserExtendedDetails_paymentDetails_history = {
  __typename: "HistoryItem",
  created: any,
  id: string,
  paid: ?number,
  total: number,
};

export type UserExtendedDetails_paymentDetails = {
  __typename: "PaymentDetails",
  id: string,
  currentSubscriptionName: ?string,
  currentSubscriptionPrice: ?number,
  lastChargeDate: any,
  nextCharge: ?any,
  history: Array<UserExtendedDetails_paymentDetails_history>,
};

export type UserExtendedDetails = {
  __typename: "User",
  id: string,
  email: string,
  isSubAccount: boolean,
  roles: Array<UserExtendedDetails_roles>,
  subAccounts: Array<UserExtendedDetails_subAccounts>,
  paymentDetails: ?UserExtendedDetails_paymentDetails,
};

/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================