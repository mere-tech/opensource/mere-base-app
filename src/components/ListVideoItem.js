/**
 * @flow
 */

import React from "react";
import { connect } from 'react-redux';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from "react-native";
import { withNavigation } from 'react-navigation';
import RNBackgroundDownloader from 'react-native-background-downloader';
import { Icon } from 'react-native-elements';
import type { CompositeVideoItem } from "../../lib/Video";
import { CheckDB } from "../lib/Video";


type Props = {
  video: CompositeVideoItem,
  showPoster: ?boolean,
  app: any,
  navigation: any,
};

const DownloadItem = ({video, app, showPoster, navigation}: Props) => (
  <View
    style={{
      flex: 1,
      flexDirection: 'row',
      marginTop: 10,
    }}
  >
    { showPoster && (
      <Image
        style={{
          flex: 1,
          // backgroundColor: 'black'
        }}
        resizeMode='contain'
        resizeMethod='resize'
        source={{uri: video.largeImage}}
      />
    )}
    <Text
      numberOfLines={1}
      style={{
        flex: 5,
        marginLeft: showPoster ? 0 : 10,
        color: app.environment.mainColor,
        fontSize: 18,
        textAlignVertical: 'center'
       }}
    >
      {video.name}
    </Text>
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 2,
      }}
    >
      <TouchableOpacity
        onPress={() => {
          console.log('VIDEO:', video);
          CheckDB(video);
        }}
        style={{
          flex: 1,
        }}
      >
        <Icon
          name='cloud-download'
          color={app.environment.mainColor}
          type='font-awesome'
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          flex: 1
        }}
        onPress={() => {
          navigation.navigate({routeName: 'Video', params: { ...video }})
        }}
      >
        <Icon
          name='play'
          color={app.environment.mainColor}
          type='font-awesome'
        />
      </TouchableOpacity>
    </View>
  </View>
)

const mapStateToProps = ({ app }: GlobalState) => ({ app });
export default connect(mapStateToProps)(withNavigation(DownloadItem));