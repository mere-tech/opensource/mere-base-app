/* eslint-disable camelcase */
/* @flow */
import React from 'react';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import { StyleSheet, View, TouchableOpacity, TouchableHighlight, ImageBackground } from 'react-native';
import { Text } from './StyledText';
import { colors } from '../styles';
import { FolderExtendedDetails_assets_subFolder } from '../lib/graphql/__generated__/FolderExtendedDetails';

type Prop = {
  item: FolderExtendedDetails_assets_subFolder,
  navigation: any,
 }
 
 const baseStyle = {
 
 };

 const merge = (a,b) => Object.assign({},a,b);
 
 class FolderItem extends React.Component<Prop> {

  state = {
    showBorder: false
  }

  render() {
    if (DeviceInfo.getDeviceType() !== 'Tv') {
      return (
        <View style={merge(baseStyle, styles.row)}>
          <View style={styles.touchItem}>
            <TouchableOpacity 
              style={{
                flex: 4,
              }}
              onPress={() => {
                this.props.navigation.navigate({routeName: 'Folder', params: { folderId: this.props.item.id }})}}
            >
              <ImageBackground resizeMethod="resize" resizeMode="contain" style={styles.backgroundItem} source={{uri: this.props.item.largeImage}} />
            </TouchableOpacity>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text bold size={18} style={{ textAlign: 'center', color: this.props.env.mainColor }} >{this.props.item.name}</Text>
              { this.props.env.showDescriptionInRow && (
                <Text numberOfLines={2} size={15} style={{ color: this.props.env.secondColor }}>{this.props.item.description}</Text>
              )}
            </View>
          </View>
        </View>
      )
    }
    return (
      <View style={merge(baseStyle, styles.row)}>
        <View style={styles.touchItem}>
          <TouchableHighlight 
            onHideUnderlay={() => {
              this.setState({ showBorder: false })
            }}
            onShowUnderlay={() => {
              this.setState({ showBorder: true })
            }}
            onPress={() => {
              this.props.navigation.navigate({routeName: 'Folder', params: { folderId: this.props.item.id }})}}
          >
            <View style={{opacity: this.state.showBorder ? 1 : 0.5}}>
              <ImageBackground
                resizeMethod="auto" 
                style={[styles.backgroundItem, { borderWidth: 2, borderRadius: 1, borderColor: this.state.showBorder ? this.props.env.mainColor : colors.transparent}]}
                source={{uri: this.props.item.largeImage}} 
              />
              <View style={{opacity: this.state.showBorder ? 1 : 0.5}}>
                <Text bold size={18} style={{ color: this.props.env.mainColor }}>{this.props.item.name}</Text>
                <Text numberOfLines={2} size={15} style={{ color: this.props.env.secondColor }}>{this.props.item.description}</Text>
              </View>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    )
}};

const styles = StyleSheet.create({
  row: {
    paddingEnd: 20,
    minWidth: 200,
    minHeight: 280,
    top: 10,
    flex: 1,
  },
  playIcon: {
    height: 57,
    width: 57,
    alignItems: 'center',
  },
  touchItem: {
    flex: 1,
    borderRadius: 10,
  },
  backgroundItem: {
    flex: 1,
    borderRadius: 10,
  },
});

const withNav = withNavigation(FolderItem)
const mapStateToProps = ({ app }) => ({ env: app.environment });
const withConnect = connect(mapStateToProps)(withNav)

export default withConnect;