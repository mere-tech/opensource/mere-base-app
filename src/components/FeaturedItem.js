/* eslint-disable camelcase */
/* @flow */
import React from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, TouchableHighlight, Image, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import TextButton from './TextButton';
import { Text } from './StyledText';

import { FolderExtendedDetails_assets_video } from '../lib/graphql/__generated__/FolderExtendedDetails';
import { colors } from '../styles';

type InputProp = {
  asset: FolderExtendedDetails_assets_video,
  navigation: any
};

class FeaturedItem extends React.Component<InputProp> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={{color: this.props.env.mainColor, paddingTop: 100, start: 30, flex: 1}} size={20}>FEATURED</Text>
        <Text style={{start: 30, flex: 1, top: -7}} white bold size={40}>{this.props.asset.name}</Text>
        { DeviceInfo.getDeviceType() !== 'Tv' && (
          <TouchableOpacity style={{start: 30, paddingBottom: 80}} onPress={() => this.props.navigation.navigate({routeName: 'Video',params: { ...this.props.asset }})}>
            <Image style={styles.playIcon} source={this.props.env.playIcon} />
          </TouchableOpacity>
          )
        }
        { DeviceInfo.getDeviceType() === 'Tv' && (          
          <TextButton
            onPress={() => this.props.navigation.navigate({routeName: 'Video',params: { ...this.props.asset }})}
            style={{start: 30, width: 100, marginBottom: 80, height: 35}}
          >
            Play
          </TextButton>
          )
      }
      </View>
)}
}

const styles = StyleSheet.create({
  container: {
    top: 30,
    backgroundColor: 'rgba(255, 255, 255, 0.0)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  playIcon: {
    flex: 1,
    top: -5,
    height: 76,
    width: 76,
  },
});

const mapStateToProps = ({ app }) => ({ env: app.environment });
const withConnect = connect(mapStateToProps)(FeaturedItem)
export default withNavigation(withConnect);