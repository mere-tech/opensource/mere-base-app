/* eslint-disable camelcase */
/* @flow */
import React from 'react';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { StyleSheet, View, TouchableOpacity, TouchableHighlight, Image, ImageBackground } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { Text } from './StyledText';
import { colors } from '../styles';
import { store } from '../redux/store';
import { SET_SELECTED_ITEM } from '../modules/AppState';
import { FolderExtendedDetails_assets_video } from '../lib/graphql/__generated__/FolderExtendedDetails';
import { PutOrUpdateVideoForDownload } from '../lib/Video';

type Prop = {
  item: FolderExtendedDetails_assets_video,
  navigation: any,
  app: any
}

class VideoItem extends React.Component<Prop> {

  state = {
    showBorder: false
  }

  render() {
    if (DeviceInfo.getDeviceType() !== 'Tv') {
      // // put some in for easier population of downloadability
      // PutOrUpdateVideoForDownload(this.props.item, "");
      return (
        <View style={styles.row}>
          <View style={styles.touchItem}>
            <TouchableOpacity
              style={{
                flex: 4,
              }}
              onPress={() => this.props.navigation.navigate({routeName: 'Video', params: { ...this.props.item }})}
            >
              <ImageBackground
                resizeMode='contain'
                resizeMethod="resize"
                style={styles.backgroundItem}
                source={{uri: this.props.item.largeImage}}
              />
            </TouchableOpacity>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text bold size={18} style={{  textAlign: 'center', color: this.props.app.environment.mainColor }}>{this.props.item.name}</Text>
              {/* <Text numberOfLines={2} size={15} style={{ color: this.props.app.environment.secondColor }}>{this.props.item.description}</Text> */}
            </View>
          </View>
        </View>
      )
    }
    return (
      <View style={styles.row}>
        <View style={styles.touchItem}>
          <TouchableHighlight
            onHideUnderlay={() => {
              this.setState({ showBorder: false })
            }}
            onShowUnderlay={() => {
              this.setState({ showBorder: true })
            }}
            style={styles.backgroundItem}
            onPress={() => {
              this.props.navigation.navigate({routeName: 'Video', params: { ...this.props.item }})}}
          >
            <View>
              <ImageBackground
                resizeMethod="resize"
                style={[styles.backgroundItem, { opacity: this.state.showBorder ? 1 : 0.5, borderWidth: 2, borderRadius: 1, borderColor: this.state.showBorder ? this.props.app.environment.mainColor : colors.transparent}]}
                source={{uri: this.props.item.largeImage}}
              />
              <View style={{ opacity: this.state.showBorder ? 1 : 0.5 }}>
                <Text bold size={18} style={{ color: this.props.app.environment.mainColor }}>{this.props.item.name}</Text>
                <Text numberOfLines={2} size={15} style={{ color: this.props.app.environment.secondColor }}>{this.props.item.description}</Text>
              </View>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  row: {
    flex: 1,
    minWidth: 200,
    minHeight: 280,
    paddingEnd: 20,
  },
  playIcon: {
    height: 57,
    width: 57,
    alignItems: 'center',
  },
  touchItem: {
    flex: 1,
    borderRadius: 10,
  },
  backgroundItem: {
    flex: 1,
    borderRadius: 10,
  },
});

const mapStateToProps = ({ app }: GlobalState) => ({ app });
export default connect(mapStateToProps)(withNavigation(VideoItem));
