import React from "react";
import { connect } from "react-redux";
import { ImageBackground, Dimensions } from "react-native";
import LinearGradient from "react-native-linear-gradient";

class AppBackground extends React.Component {
  state = {
    height: 0,
    width: 0
  };

  componentWillMount() {
    Dimensions.addEventListener("change", this.handleDimensionChange);
    const { height, width } = Dimensions.get("window");
    this.setState({ height, width });
  }

  componentWillUnmount() {
    Dimensions.removeEventListener("change", this.handleDimensionChange);
  }

  handleDimensionChange = ({ window }) => {
    const { height, width } = window;
    this.setState({ height, width });
  };

  render() {
    const { app, backgroundImage, backgroundImageResource } = this.props;
    const { width, height } = this.state;
    let toSet = { width, height };
    // If in landscape mode we want to change it up a little
    if (width > height) {
      toSet = { width, height };
    }

    let toSetImage = {};

    if (backgroundImageResource) {
      toSetImage = backgroundImageResource;
    } else if (backgroundImage) {
      toSetImage = { uri: backgroundImage };
    } else if (app.backgroundImageUri) {
      toSetImage = { uri: app.backgroundImageUri };
    }

    const prefix = app.environment.backgroundColor;

    return (
      <ImageBackground
        resizeMode="cover"
        resizeMethod="scale"
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          width: toSet.width,
          height: '100%',
          backgroundColor: "transparent"
        }}
        source={toSetImage}
      >
        {width > height && (
          <LinearGradient
            start={{ x: 0.7, y: 0.0 }}
            end={{ x: 1, y: 0 }}
            colors={[`${prefix}88`, `${prefix}FF`]}
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              width: toSet.width,
              height: toSet.height,
              backgroundColor: "transparent"
            }}
          />
        )}
        <LinearGradient
          colors={[`${prefix}00`, `${prefix}00`, `${prefix}22`, `${prefix}FF`]}
          style={{
            backgroundColor: "transparent",
            flex: 1
          }}
        />
      </ImageBackground>
    );
  }
}

const mapStateToProps = ({ app }) => ({ app });
export default connect(mapStateToProps)(AppBackground);
