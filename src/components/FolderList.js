/* eslint-disable camelcase */
/* @flow */
import React from 'react';
import { ScrollView, View } from 'react-native';
import HorizontalScrollView from './HorizontalScrollView';
import VideoItem from './VideoItem';
import FolderItem from './FolderItem';

import type { FolderExtendedDetails } from '../lib/graphql/__generated__/FolderExtendedDetails';
import type { GridFolderDetails_assets_subFolder } from '../lib/graphql/__generated__/GridFolderDetails';

type InputProp = {
  folder: FolderExtendedDetails | GridFolderDetails_assets_subFolder,
};

export default ({ folder, ...rest }: InputProp) => (
  <ScrollView
    overScrollMode='never'
  >
    <HorizontalScrollView
      viewStyle={{
        height: '100%',
        width: '100%',
        start: 0,
      }}
      removeClippedSubviews
      showTitle
      showSubTitle
      title={folder.name}
      subTitle={folder.description}
      {...rest}
    >
      <View style={{width: 30}} />
      { folder.assets
        && folder.assets.map( fass => {
          if (fass && fass.video) {
            return <VideoItem key={fass.id} item={fass.video} />;
          }
          if (fass && fass.subFolder) {
            return <FolderItem key={fass.id} item={fass.subFolder} />;
          }
          return null;          
        })
      }
    </HorizontalScrollView>
  </ScrollView>
)