/* @flow */
import React from 'react';
import { Image, FlatList, Text, TouchableHighlight, StyleSheet, View, Animated } from 'react-native';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import { colors } from '../styles';
import { GlobalState } from '../redux/reducer';
const iconHome = require('../../assets/images/tabbar/home-2.png');
const iconProfile = require('../../assets/images/tabbar/login-4.png');
const iconPages = require('../../assets/images/tabbar/login-4.png');
const iconComponents = require('../../assets/images/tabbar/login-4.png');
const iconBook = require('../../assets/images/tabbar/calendar.png');
const iconRadio = require('../../assets/images/tabbar/radio.png');

const styles = StyleSheet.create({
  drawerItem: {
    marginTop: 10,
    marginBottom: 10,
    height: 40,
    flex: 1,
    flexDirection: 'row',
  },
  icon: {
    width: 24,
    height: 24,
  },
});

class SideNav extends React.Component {

  state = {
    minWidth: 10,
    maxWidth: 130,
    expanded: false,
    animation: new Animated.Value(10)
  }

  toggle() {
    let initialValue    = this.state.expanded? this.state.maxWidth + this.state.minWidth : this.state.minWidth;
    let finalValue      = this.state.expanded? this.state.minWidth : this.state.maxWidth + this.state.minWidth;

    this.setState({
        expanded : !this.state.expanded  
    });

    this.state.animation.setValue(initialValue);  
    
    Animated.spring(    
        this.state.animation,
        {
            toValue: finalValue
        }
    ).start(); 
  }

  close() {
    this.setState({
        expanded : false  
    });

    this.state.animation.setValue(this.state.maxWidth);  
    
    Animated.spring(    
        this.state.animation,
        {
            toValue: this.state.minWidth
        }
    ).start(); 
  }

  render() {
    if (DeviceInfo.getDeviceType() !== "Tv") {
      return (<View />)
    } 
    const { environment } = this.props.app;

    const parent = this.props.navigation.dangerouslyGetParent();

    const stateIndex = parent.state.index;
    const currentIndex = parent.state.routes[stateIndex];

    const navItems = parent.state.routes;

    let focused = null;
    return (
      <Animated.View style={{ width: this.state.animation }}>
        <FlatList
          style={{paddingTop: 100, height: '100%', width: '100%', backgroundColor: colors.darkGray}}
          data={navItems}
          renderItem={(item) => {
            let iconSource;
            switch (item.item.routeName) {
              case 'Home':
                iconSource = iconHome;
                break;
              case 'Calendar':
                iconSource = iconProfile;
                break;
              case 'Web':
                iconSource = iconBook;
                break;
              case 'Pages':
                iconSource = iconPages;
                break;
              case 'Radio':
                iconSource = iconRadio;
                break;
              case 'Components':
                iconSource = iconComponents;
                break;
              default:
                iconSource = iconComponents;
            }

            return (
              <TouchableHighlight 
                activeOpacity={0.9}
                underlayColor={environment.thirdColor}
                onShowUnderlay={() => { 
                  focused = item.item;
                  if (!this.state.expanded) {         
                    this.toggle();
                  }
                }}
                onHideUnderlay={() => {
                  focused = null;
                  setTimeout(() => {
                    if (focused === null) {
                      this.toggle();
                    }
                  }, 500);
                }}
                onPress={() => {
                  this.props.navigation.navigate(item.item.key)
                }}
              >
                <View
                  style={styles.drawerItem}
                >
                  <View style={{ alignSelf: 'center', backgroundColor: item.item === currentIndex ? environment.mainColor : colors.transparent, height: 40, width: this.state.expanded ? 0 : 10 }} />
                  <Image
                    resizeMode="contain"
                    source={iconSource}
                    style={[styles.icon, { marginStart: 10, alignSelf: 'center', tintColor: item.item === currentIndex ? environment.mainColor : environment.secondColor }]}
                  />
                  <Text
                    style={{ alignSelf: 'center', marginStart: 20, width: 60, color: item.item === currentIndex ? environment.mainColor : environment.secondColor }}
                  >
                    {item.item.key}
                  </Text>
                  <View style={{ marginStart: 20, alignSelf: 'flex-end', backgroundColor: item.item === currentIndex ? environment.mainColor : colors.transparent, height: 40, width: 10 }} />
                </View>
              </TouchableHighlight>
            )
          }}
        />
      </Animated.View>
)}};

const mapStateToProps = ({ app }: GlobalState) => ({ app });
export default connect(mapStateToProps)(withNavigation(SideNav))
