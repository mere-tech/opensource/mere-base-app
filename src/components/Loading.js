/* @flow */
import React from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity } from 'react-native';
import { Ionicons } from 'react-native-ionicons';
import { Query } from 'react-apollo';
import { BallIndicator, MaterialIndicator } from 'react-native-indicators';

const styles = {
  container: {
    width: "100%",
    height: "100%",
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorText: {
    color: '#FFFFFF',
    fontSize: 15,
  },
  activityIndicator: {
    height: 100,
  },
};

type LoadingProp = {
  children: any,
  query:  any,
  variables: any,
  app: any,
};

const getVariables = (variables, { environment }) => Object.assign(
  {},
  { rnId:  environment.rnId },
  variables
);

class LoadingHandler extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showSlowText: false, showAdd: false };
  }

  componentDidMount = () => {
    // Remember the timer handle
    this.timerHandle = setTimeout(() => {
      this.setState({ showSlowText: true });
      this.timerHandle = 0;
    }, 8000);
    this.showAddTimerHandle = setTimeout(() => {
      this.setState({ showAdd: true });
      this.showAddTimerHandle = 0;
    }, 14000);
  };

  componentWillUnmount = () => {
    if(this.timerHandle) {
      clearTimeout(this.timerHandle);
      this.timerHandle = 0;
    }
    if(this.showAddTimerHandle) {
      clearTimeout(this.showAddTimerHandle);
      this.showAddTimerHandle = 0;
    }    
  }

  render() {
    const { refetch } = this.props;
    const { app } = this.props;
    const { showSlowText, showAdd } = this.state;

    return (
      <View style={Object.assign({}, styles.container, { backgroundColor: app.environment.backgroundColor })}>
        { !showAdd && (
          <View style={styles.activityIndicator}>
            { app.environment.indicator === 'material' && (
              <MaterialIndicator size={80} color={app.environment.mainColor} />
            )}
            { app.environment.indicator !== 'material' && (
              <BallIndicator size={80} color={app.environment.mainColor} />
            )}
          </View>
        )}
        { showAdd && (
          <Text style={styles.errorText}>
            Please check your internet settings and come back.
          </Text>
        )}
        { !showAdd && showSlowText && (
          <>
            <Text style={styles.errorText}>
              Your connection seems to be slow.
            </Text>
            <Text style={styles.errorText}>
              Your experience may be degraded.
            </Text>
          </>
        )}
      </View>
    );
  }

}

const QueryWrap = ({ pollInterval, variables, query, app, children, ...rest }: LoadingProp) => (
  <Query
    query={query}
    variables={getVariables(variables, app)}
    pollInterval={ pollInterval ? pollInterval : 60000}
  >
    {({ loading, data, error, refetch}) => {

      if (error && error.networkError) {
        console.log("Error reaching communication");
        return (
          <View style={styles.container}>
            <Text style={styles.errorText}>
              There was a problem communicating with the internet. Tap
            </Text>
            <TouchableOpacity
              onPress={() => refetch()} >
              <Ionicons name="md-refresh" size={68} color='#C67700' />
            </TouchableOpacity>BallIndicator
            <Text style={styles.errorText}>            
              To try again.
            </Text>
          </View>
        )
      }
      if ( loading || !data ) {
        return <LoadingHandler refetch={refetch} app={app} />;
      }
      return children(Object.assign({}, {data}, rest));
    }}
  </Query>
);
const mapStateToProps = ({ app }) => ({ app });
const ConnectedQueryWrap = connect(mapStateToProps)(QueryWrap);

export default ConnectedQueryWrap;