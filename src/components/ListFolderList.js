/**
 * @flow
 */

import React from "react";
import { connect } from 'react-redux';
import {
  Image,
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Text
} from "react-native";
import { withNavigation } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import AppBackground from "./AppBackground";
import LinearGradient from "react-native-linear-gradient";
import VideoItem from "./ListVideoItem";
import type { CompositeVideoItem } from "../../lib/Video";
import { pSBC } from "../lib/Color";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  scrollContainer: {
    flex: 1,
  }
});

class ListFolderList extends React.Component {

  render() {
    const { app, folder, navigation } = this.props;
    const { environment } = app;
    const { thirdColor, mainColor } = environment;
    const darkerColor = pSBC(-0.8, thirdColor);
 
    return (
      <View style={styles.container}>
        { DeviceInfo.getDeviceType() !== "Tv" && (
          <TouchableOpacity
            style={{ position: "absolute", top: 30, start: 30, height: 50, width: 50, zIndex: 999}} 
            onPress={() => navigation.goBack(null)}
          >
            <Image resizeMode="center" style={{ width: 30, height: 30 }} source={require('../../assets/images/icons/arrow-back.png')} />
          </TouchableOpacity>
        )}  
        <AppBackground />
        <ScrollView style={styles.scrollContainer} contentContainerStyle={{flexGrow: 1}}>
          <Text style={{ marginTop: 50, marginStart: 30, fontSize: 31, color: mainColor, fontWeight: 'bold'}} >
            {/* {folder.name} */}
          </Text>
          <View style={{ flexDirection: 'row', height: 300 }}>
            <Image
              resizeMode='contain'
              resizeMethod='resize'
              source={{uri: folder.largeImage}}
              style={{
                flex: 3,
                margin: 10,
              }}
            />
            { folder.description && folder.description !== "null" && (
              <Text
                style={{
                  margin: 10,
                  flex: 2,
                  fontSize: 18,
                  color: mainColor
                }}
              >
                {folder.description}
              </Text>
            )}

          </View>
          <View style={{ marginTop: 20, flexGrow: 1 }}>
            <LinearGradient
              colors={[thirdColor, darkerColor]}
              style={{ flexGrow: 1 }}
            >
              { folder.assets.map(item => <VideoItem {...item} key={item.id} />)}
            </LinearGradient>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ app }: GlobalState) => ({ app });
export default connect(mapStateToProps)(withNavigation(ListFolderList));