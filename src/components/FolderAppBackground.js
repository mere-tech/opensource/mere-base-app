/* @flow */
import React from 'react';
import DeviceInfo from 'react-native-device-info';
import AppBackground from './AppBackground';

export default ({ folder }) => <AppBackground backgroundImage={DeviceInfo.getDeviceType() === "Tv" ? folder.tvReactNativeBackgroundImageUri : folder.mobileBackgroundImageUri} />;
