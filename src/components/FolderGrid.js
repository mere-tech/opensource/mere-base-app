/* @flow */
import React from 'react';
import { ScrollView } from 'react-native';
import FolderList from './FolderList';

import type { GridFolderDetails } from '../lib/graphql/__generated__/GridFolderDetails';

type InputProp = {
  folder: GridFolderDetails,
  navigation: any,
};

export default ({ folder, ...rest }: InputProp) => (
  <ScrollView removeClippedSubviews>
    { folder.assets
      && folder.assets.map( fass => {
        if (fass && fass.subFolder) {
          return (
            <FolderList
              {...rest}
              folder={fass.subFolder}
              key={fass.id}
            />
          );
        }
        return null;          
      })
    }
  </ScrollView>
)