/* eslint-disable camelcase */
/* @flow */
import React from 'react';
import { connect } from 'react-redux';
import { ScrollView, StyleSheet, View, TouchableOpacity, Image } from 'react-native';
import { withNavigation } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import VideoItem from './VideoItem';
import FolderItem from './FolderItem';
import { Text } from './StyledText';

import type { FolderExtendedDetails } from '../lib/graphql/__generated__/FolderExtendedDetails';
import type { GridFolderDetails_assets_subFolder } from '../lib/graphql/__generated__/GridFolderDetails';
import FolderAppBackground from './FolderAppBackground';

type InputProp = {
  navigation: any,
  env: any,
  folder: FolderExtendedDetails | GridFolderDetails_assets_subFolder,
};

const VerticalFolderList = ({ navigation, folder, env }: InputProp) => (
  <View>
    { DeviceInfo.getDeviceType() !== "Tv" && (
      <TouchableOpacity
        style={{ position: "absolute", top: 30, start: 30, height: 50, width: 50, zIndex: 999}} 
        onPress={() => navigation.goBack(null)}
      >
        <Image resizeMode="center" style={{ width: 30, height: 30 }} source={require('../../assets/images/icons/arrow-back.png')} />
      </TouchableOpacity>
    )}
    <ScrollView style={styles.container} removeClippedSubviews overScrollMode='never'>
      <FolderAppBackground folder={folder} />
      <Text style={{ color: env.mainColor, top: 100, paddingHorizontal: 30}} bold size={25}>{folder.name}</Text>
      <Text style={{ color: env.secondColor, top: 110, paddingHorizontal: 30}} numberOfLines={2} size={18}>{folder.description}</Text>
      <View style={{ top: 150, paddingHorizontal: 30, paddingBottom: 90, flex: 1, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}>
        { folder.assets
          && folder.assets.map( fass => {
            if (fass && fass.video) {
              return <VideoItem itemStyle={styles.item} key={fass.id} item={fass.video} />;
            }
            if (fass && fass.subFolder) {
              return <FolderItem key={fass.id} item={fass.subFolder} />;
            }
            return null;          
          })
        }
      </View>
    </ScrollView>
  </View>
)
//      <FolderAppBackground folder={folder} />

const styles = StyleSheet.create({
  item: {

  },
  container: {
    top: 0,
    bottom: 0,
    height: '100%',
    width: '100%',

    backgroundColor: 'transparent',
  },
})

const withNav = withNavigation(VerticalFolderList)
const mapStateToProps = ({ app }) => ({ env: app.environment });
const withConnect = connect(mapStateToProps)(withNav)

export default withConnect;