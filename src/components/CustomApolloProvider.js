/* @flow */
import * as React from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { setContext } from "apollo-link-context";
import { InMemoryCache } from "apollo-cache-inmemory";
import type { AuthState } from "../reducers/auth";
import { store } from "../redux/store";

const createClient = (uri, token) => {
  const httpLink = createHttpLink({ uri });
  const authLink = setContext((_, { headers }) => ({
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ""
    }
  }));
  const client = new ApolloClient({
    link: token ? authLink.concat(httpLink) : httpLink,
    cache: new InMemoryCache()
  });
  return client;
};

const currentClientMapping = {};
// We want to go through and check to see if we already have the client so we don't
// endlessly create the client every time.
const createOrGetClient = (uri, jwt) => {
  if (jwt && jwt !== "") {
    if (!currentClientMapping[jwt]) {
      currentClientMapping[jwt] = createClient(uri, jwt);
    }
    return currentClientMapping[jwt];
  }
  if (!currentClientMapping.nojwt) {
    currentClientMapping.nojwt = createClient(uri);
  }
  return currentClientMapping.nojwt;
};

type InputProps = {
  auth: AuthState,
  app: AppState,
  children: any
};

const WrappedMainView = ({ app, auth, children }: InputProps) => {
  if (!auth.isLoaded) {
    return <View />;
  }
  if (app && auth.jwt && auth.isLoaded && auth.isAuthenticated) {
    return (
      <ApolloProvider
        client={createOrGetClient(app.environment.graphqlEndpoint, auth.jwt)}
      >
        {children}
      </ApolloProvider>
    );
  }
  return (
    <ApolloProvider client={createOrGetClient(app.environment.graphqlEndpoint)}>
      {children}
    </ApolloProvider>
  );
};
const mapStateToProps = ({ app, auth }) => ({ app, auth });
export default connect(mapStateToProps)(WrappedMainView);
