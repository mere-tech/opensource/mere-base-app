/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: AppDetails
// ====================================================

export type AppDetails_reactNative = {
  __typename: "ReactNative",
  auth0Audience: ?string,
  auth0ClientId: ?string,
  auth0Domain: ?string,
  id: ?string,
  brandingImageUri: ?string,
  backgroundImageUri: ?string,
  primaryColor: ?string,
  secondaryColor: ?string,
};

export type AppDetails = {
  reactNative: ?AppDetails_reactNative
};

export type AppDetailsVariables = {
  rnId: string
};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================