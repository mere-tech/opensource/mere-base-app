import React from "react";
import { View, ImageBackground } from "react-native";
import { loader } from "graphql.macro";
import { connect } from "react-redux";
import Orientation from "react-native-orientation-locker";
import DeviceInfo from "react-native-device-info";
import SplashScreen from 'react-native-splash-screen'
import Loading from "../components/Loading";
import { store } from "../redux/store";
import type { GlobalState } from "../redux/reducer";
import { APP_LOADED } from "./AppState";

const query = loader("./App.graphql");

const styles = {
  backgroundImage: {
    backgroundColor: "rgba(52, 0, 0, 0.8)",
    position: "absolute",
    height: "100%",
    width: "100%"
  }
};
class AppView extends React.Component {
  componentDidMount() {
    SplashScreen.hide();
  }

  componentWillMount() {
    const { data } = this.props;
    store.dispatch({
      type: APP_LOADED,
      payload: {
        auth0Audience: data.reactNative.auth0Audience,
        auth0ClientId: data.reactNative.auth0ClientId,
        auth0Domain: data.reactNative.auth0Domain,
        brandingImageUri: data.reactNative.brandingImageUri,
        backgroundImageUri: data.reactNative.backgroundImageUri,
        mainColor: data.reactNative.primaryColor,
        secondColor: data.reactNative.secondaryColor
      }
    });
  }

  render() {
    const { app, children } = this.props;

    const isTablet = DeviceInfo.isTablet();
    if (!isTablet && DeviceInfo.getDeviceType() !== "Tv") {
      Orientation.lockToPortrait();
    } else {
      Orientation.unlockAllOrientations();
    }
    return children;
  }
}

const mapStateToProps = ({ app }: GlobalState) => ({ app });
const ConnectedAppView = connect(mapStateToProps)(AppView);

export default ({ ...rest }) => (
  <Loading query={query}>
    {({ data }) => <ConnectedAppView {...rest} data={data} />}
  </Loading>
);
