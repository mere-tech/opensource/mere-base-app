import { compose, withState } from 'recompose';
import Auth0LoginScreen from './Auth0Login';

export default compose(withState(false))(
  Auth0LoginScreen,
);
