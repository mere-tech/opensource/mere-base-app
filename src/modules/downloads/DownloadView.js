/**
 * @flow
 */

import React from "react";
import { connect } from 'react-redux';
import {
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  Text
} from "react-native";
import RNBackgroundDownloader from 'react-native-background-downloader';
import LinearGradient from "react-native-linear-gradient";
import DownloadItem from "../../components/ListVideoItem";
import AppBackground from "../../components/AppBackground";
import { GetAllVideos, CheckDB, SubscribeToAll } from "../../lib/Video";
import type { CompositeVideoItem } from "../../lib/Video";
import { pSBC } from "../../lib/Color";


const playbackTimeInSeconds = 172800;
const timeToAllowForDownloadInSeconds = 1209600;

const SecondsToString = (seconds) => {
  const hours   = Math.floor(seconds/ 3600);
  const minutes = Math.floor((seconds- (hours * 3600)) / 60);

  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  return hours + ':' + minutes;
}


// Map function to allow the video objects to have the time left
// mapped for display
const MapTimeInVideos = (video: CompositeVideoItem) => {
  if (video.firstWatchedSinceAuthorization && video.partiallyWatched) {
    const diff = new Date().getTime() - video.firstWatchedSinceAuthorization.getTime();
    const diffInSeconds = dif / 1000;
    const timeLeftToWatchString = SecondsToString(diffInSeconds);
    expired = diffInSeconds > playbackTimeInSeconds;
    return Object.assign(video, {expired, timeLeftToWatchString });
  } else if (video.firstWatchedSinceAuthorization) {
    const diff = new Date().getTime() - video.firstWatchedSinceAuthorization.getTime();
    const diffInSeconds = dif / 1000;
    const timeLeftToWatchString = SecondsToString(diffInSeconds);
    expired = diffInSeconds > timeToAllowForDownloadInSeconds;
    return Object.assign(video, {expired, timeLeftToWatchString });
  }
  return Object.assign(video, {expired: false, timeLeftToWatchString: "N/A"});
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  scrollContainer: {
    flex: 1,
  }
});

type State = {
  available: array<CompositeVideoItem>,
  expired: array<CompositeVideoItem>,
  hasAny: boolean,
  changes: any,
}; 

class Download extends React.Component<any, State> {
  state = {
    available: [],
    expired: [],
    hasAny: false,
    changes: null,
  };

  async componentDidMount() {
    const videos = await GetAllVideos();
    const updateVideos = (videos) => {
      const toSet = videos.map(MapTimeInVideos)
        .reduce((acc, current: CompositeVideoItem) => {
          if (current.expired) {
            acc.expired.push(current);
          } else {
            acc.available.push(current);
          }
          return acc;
      }, {available: [], expired: []})
      this.setState(Object.assign(toSet, {hasAny: videos.length > 0}));
    }
    updateVideos(videos);
    this.setState({
      changes: CheckDB(() => {
        const newVideos = GetAllVideos();
        updateVideos(videos);
      })
    });
  }


  render() {
    const { app } = this.props;
    const { environment } = app;
    const { thirdColor, mainColor } = environment;
    const { available, expired, hasAny } = this.state;
    const darkerColor = pSBC(-0.8, thirdColor);
    return (
      <View style={styles.container}>
        <AppBackground />
        <ScrollView style={styles.scrollContainer} contentContainerStyle={{flexGrow: 1}}>
          <View style={{ height: 300 }} />
          <Text style={{ marginStart: 30, fontSize: 31, color: mainColor, fontWeight: 'bold'}} >
            Downloaded Items
          </Text>
          <View style={{ marginTop: 20 }}>
            <LinearGradient
              colors={[thirdColor, darkerColor]}
              // style={{ height: "100%" }}
            >
              {
                hasAny
                && (
                  <>
                    { available.length > 0 && (
                      <>
                        {/* <Text style={{ marginStart: 30, fontSize: 24, fontWeight: 'bold', color: mainColor }}>Available</Text> */}
                        {
                          available.map(item => <DownloadItem showPoster video={item} key={item._id} />)
                        }
                      </>
                    )}
                    { expired.length > 0 && (
                      <>
                        <Text bold size={18} style={{  marginStart: 30, fontWeight: 'bold', color: mainColor }}>Expired</Text>
                        {
                          expired.map((item: CompositeVideoItem) => <Text style={{ color: mainColor }} key={item._id}>{item.name}</Text>)
                        }
                      </>
                    )}
                  </>
                )
              }
              {
                !hasAny && (
                  <Text>
                    No content available for playback
                  </Text>
                )
              }
              <View style={{ height: 400 }} />
            </LinearGradient>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ app }: GlobalState) => ({ app });
export default connect(mapStateToProps)(Download);
