import { compose, withState } from 'recompose';
import FolderScreen from './FolderView';

export default compose(withState(false))(
  FolderScreen,
);
