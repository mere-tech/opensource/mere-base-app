import React from "react";
import { View, StatusBar, StyleSheet } from "react-native";
import { loader } from "graphql.macro";
import { connect } from 'react-redux';
import { withNavigation } from "react-navigation";
import Loading from "../../components/Loading";
import VerticalFolderList from "../../components/VerticalFolderList";
import ListFolderList from "../../components/ListFolderList";
import AppBackground from "../../components/AppBackground";

const query = loader("./Folder.graphql");

const FolderView = ({ navigation, app }: { navigation: any }) => (
  <Loading
    query={query}
    variables={{ folderId: navigation.getParam("folderId") }}
  >
    {({ data }) => (
      <View style={styles.container}>
        <StatusBar hidden />
        <AppBackground />
        { !app.environment.listViewForFolder && (<VerticalFolderList folder={data.reactNative.folder} />)}
        { app.environment.listViewForFolder && (<ListFolderList folder={data.reactNative.folder} />)}
      </View>
    )}
  </Loading>
);

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

// back button on video
// automatically go back on video finish
// grid view on landscape in folder view
// switch logo on portrait vs landscape
const mapStateToProps = ({ app }) => ({ app });
export default connect(mapStateToProps)(withNavigation(FolderView));
