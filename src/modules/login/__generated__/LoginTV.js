/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: LoginTV
// ====================================================

export type LoginTV_reactNative_loginDetails = {
  __typename: "LoginDetails",
  loginUri: string,
  qrUri: string,
  quickCode: string,
};

export type LoginTV_reactNative = {
  __typename: "ReactNative",
  id: ?string,
  loginDetails: ?LoginTV_reactNative_loginDetails,
};

export type LoginTV = {
  reactNative: ?LoginTV_reactNative
};

export type LoginTVVariables = {
  rnId: string
};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================