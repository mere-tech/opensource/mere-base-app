import React from 'react';
import { connect } from 'react-redux';
import { store } from '../../redux/store';
import { View, StatusBar, StyleSheet, Text, Image } from 'react-native';
import { loader } from 'graphql.macro';
import AuthCheck from './AuthHandler';
import { withNavigation } from 'react-navigation';
import Loading from '../../components/Loading';
import TextButton from '../../components/TextButton';
import SideNav from '../../components/SideNav';
import { AUTH_LOGOUT } from '../../reducers/auth';
import VerticalFolderList from '../../components/VerticalFolderList';

const query = loader('./GetDetails.graphql');

const LoginView = ({ navigation, auth, env }: {navigation: any,  auth: any, env: any}) => {
 if (auth.isLoaded && auth.isAuthenticated) {
  return (
    <View style={styles.parent}>
      <SideNav />
      <View x={console.log(auth)} style={styles.container}>
        <Text bold size={18} style={{ color: env.mainColor }}>You are currently Logged in</Text>
        <TextButton
            onPress={() => {
              store.dispatch({
                type: AUTH_LOGOUT,
              });
            }}
            style={{width: 100, marginBottom: 80, height: 35}}
          >
            Logout
          </TextButton>
      </View>
    </View>
  )
 }
 return (
    <Loading
      query={query}
      pollInterval={600000}
      variables={{ folderId: navigation.getParam('folderId') }}
    >
      {({ data }) => (
        <View style={styles.parent}>
          <AuthCheck codeToCheck={data.reactNative.loginDetails.quickCode} />
          <SideNav />
          <View x={console.log(auth)} style={styles.container}>
            <StatusBar hidden />
            <Text bold size={18} style={{ color: env.mainColor }}>To login, go to {data.reactNative.loginDetails.loginUri}</Text>
            <Text bold size={18} style={{ color: env.mainColor }}>enter the code {data.reactNative.loginDetails.quickCode}</Text>
            <Text />
            <Text bold size={18} style={{ color: env.mainColor }}>or</Text>
            <Text />
            <Image
              style={{
                width: 200,
                height: 200
              }}
              source={{ uri: data.reactNative.loginDetails.qrUri }}
            />
          </View>
        </View>
      )}
    </Loading>
  );
}

const styles = StyleSheet.create({
  parent: {
    flex: 1,
    flexDirection: 'row',
  },
  container: {
    backgroundColor: '#000000',
    height: "100%",
    width: "100%",
    justifyContent: 'center',
    alignItems: 'center',
  }
})

// back button on video
// automatically go back on video finish
// grid view on landscape in folder view
// switch logo on portrait vs landscape


const withNav = withNavigation(LoginView);
const mapStateToProps = ({ app, auth }) => ({ auth, env: app.environment });
const withConnect = connect(mapStateToProps)(withNav);

export default withConnect;