import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import { BallIndicator } from 'react-native-indicators';
import Video from 'react-native-video';
import DeviceInfo from 'react-native-device-info';
import MusicControl from 'react-native-music-control';

import { fonts, colors } from '../../styles';

export default class VideoScreen extends React.Component {
  state = {
    paused: false,
  }

  constructor(props){
    super(props);
 
    const item = this.props.navigation.state.params;

    MusicControl.setNowPlaying({
      title: item.name,
      artwork: item.largeImage, 
      artist: item.name,
      duration: 294, 
      description: '', 
    });
    MusicControl.on('play', this.handlePlay);  
    MusicControl.on('pause', this.handlePause);
  }

  handlePlay = () => {
    this.setState({ paused: false });
    MusicControl.updatePlayback({
      state: MusicControl.STATE_PLAYING,
    })
  }

  handlePause = () => {
    this.setState({ paused: true });
    MusicControl.updatePlayback({
      state: MusicControl.STATE_PAUSED,
    })
  }

  render() {
    return (
      <View style={styles.container}>
        { DeviceInfo.getDeviceType() !== "Tv"
          && !this.props.navigation.state.params.hideBack
          && (
          <TouchableOpacity
            style={{ position: "absolute", top: 80, start: 30, height: 50, width: 50, zIndex: 999}} 
            onPress={() => this.props.navigation.goBack(null)}
          >
            <Image resizeMode="center" style={{ width: 30, height: 30 }} source={require('../../../assets/images/icons/arrow-back.png')} />
          </TouchableOpacity>
        )}
        { !this.props.readyToPlay && (
          <BallIndicator 
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
            }}
            size={80} 
            color={this.props.app.environment.mainColor} 
          />
        )}
        <Video
          source={{uri: this.props.navigation.state.params.hls}}
          ref={this.videoPlayer}
          controls={this.props.navigation.state.params.__typename === "Video"}
          poster={this.props.navigation.state.params.__typename === 'Video' ? this.props.navigation.state.params.largeImage : ''}
          audioOnly={this.props.navigation.state.params.__typename === 'Audio'}
          paused={this.state.paused}       
          onEnd={() => {
            this.props.navigation.goBack(null);
          }}
          onProgress={(progress) => {
            this.props.setReadyToPlayState(true);
            MusicControl.updatePlayback({
              state: this.state.paused ? MusicControl.STATE_PAUSED : MusicControl.STATE_PLAYING,
              elapsedTime: progress.currentTime,
              bufferedTime: progress.playableDuration,
            })
          }}
          onReadyForDisplay={() => {
            this.props.setReadyToPlayState(true)
          }}
          ignoreSilentSwitch="ignore"
          playInBackground={DeviceInfo.getDeviceType() !== 'Tv'}
          playWhenInactive={DeviceInfo.getDeviceType() !== 'Tv'}
          allowsExternalPlayback={DeviceInfo.getDeviceType() !== 'Tv'}
          style={styles.backgroundVideo}
        />
        { this.props.navigation.state.params.__typename === "Audio" && (
          <Image
            resizeMethod="auto"
            resizeMode="contain"
            style={{ height: '40%', width: '100%' }}
            source={require("../../../assets/images/radio.png")}
          />
        )}
        { this.props.navigation.state.params.__typename === "Audio" && (
          <TouchableOpacity
            style={{ 
              height: 50,
              width: '100%',
              position: 'absolute',
              left: 0,
              right: 0,
              bottom: 0,
              marginBottom: 40
            }} 
            onPress={() => { 
              if (this.state.paused) {
                this.handlePlay();
              } else {
                this.handlePause();
              }
            }}
          >
            <Image
              resizeMethod="auto"
              resizeMode="contain"
              style={{ 
                height: '100%',
                width: '100%',
                tintColor: this.props.app.environment.mainColor
              }}
              source={this.state.paused ? require("../../../assets/images/outline_play.png") : require("../../../assets/images/outline_pause.png")}
            />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  activityIndicator: {
    flex: 1,
  },
  container: {
    backgroundColor: colors.black,
    flex: 1,
    justifyContent: 'space-around',
  },
  nerdImage: {
    width: 80,
    height: 80,
  },
  availableText: {
    color: colors.white,
    fontFamily: fonts.primaryRegular,
    fontSize: 40,
    marginVertical: 3,
  },
  textContainer: {
    alignItems: 'center',
  },
  buttonsContainer: {
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  button: {
    alignSelf: 'stretch',
    marginBottom: 20,
  },
});
