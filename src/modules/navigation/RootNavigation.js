import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';

import MainTabNavigator from './MainTabNavigator';

import VideoPlayer from '../video/VideoPlayerViewContainer';
import FolderView from '../folder/FolderViewContainer';

import { colors, fonts } from '../../styles';
import MainDrawerNavigator from './MainDrawerNavigator';

const stackNavigator = createStackNavigator(
  {
    Main: {
      screen: MainDrawerNavigator,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    defaultNavigationOptions: () => ({
      titleStyle: {
        fontFamily: fonts.primaryLight,
      },
      headerStyle: {
        backgroundColor: colors.primary,
        borderBottomWidth: 0,
      },
      headerTitleStyle: {
        color: colors.white,
        fontFamily: fonts.primaryRegular,
      },
      headerTintColor: '#222222',
      headerLeft: props => (
        <TouchableOpacity
          onPress={props.onPress}
          style={{
            paddingLeft: 25,
          }}
        >
          <Image
            source={require('../../../assets/images/icons/arrow-back.png')}
            resizeMode="contain"
            style={{
              height: 20,
            }}
          />
        </TouchableOpacity>
      ),
    }),
  },
);

export default createAppContainer(stackNavigator);
