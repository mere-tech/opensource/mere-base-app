import { compose, withState, lifecycle } from 'recompose';
import { withNavigation } from 'react-navigation';
import HomeScreen from './HomeView';

export default withNavigation(compose(
  withState(false),
  lifecycle({    
    componentDidMount() {
 
    },
    componentWillUnmount() {
    },
  }),
)(HomeScreen));