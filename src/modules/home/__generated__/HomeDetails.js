/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: HomeDetails
// ====================================================

export type HomeDetails_reactNative_folder_assets_subFolder_assets_video = {
  __typename: "Video",
  id: string,
  smallImage: ?string,
  largeImage: ?string,
  name: ?string,
  hd: ?string,
  hls: ?string,
  description: ?string,
};

export type HomeDetails_reactNative_folder_assets_subFolder_assets_subFolder_assets_video = {
  __typename: "Video",
  id: string,
  smallImage: ?string,
  largeImage: ?string,
  name: ?string,
  hd: ?string,
  hls: ?string,
  description: ?string,
};

export type HomeDetails_reactNative_folder_assets_subFolder_assets_subFolder_assets_subFolder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
};

export type HomeDetails_reactNative_folder_assets_subFolder_assets_subFolder_assets = {
  __typename: "Asset",
  video: ?HomeDetails_reactNative_folder_assets_subFolder_assets_subFolder_assets_video,
  subFolder: ?HomeDetails_reactNative_folder_assets_subFolder_assets_subFolder_assets_subFolder,
  order: number,
  id: string,
};

export type HomeDetails_reactNative_folder_assets_subFolder_assets_subFolder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
  assets: Array<HomeDetails_reactNative_folder_assets_subFolder_assets_subFolder_assets>,
};

export type HomeDetails_reactNative_folder_assets_subFolder_assets = {
  __typename: "Asset",
  video: ?HomeDetails_reactNative_folder_assets_subFolder_assets_video,
  subFolder: ?HomeDetails_reactNative_folder_assets_subFolder_assets_subFolder,
  order: number,
  id: string,
};

export type HomeDetails_reactNative_folder_assets_subFolder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
  assets: Array<HomeDetails_reactNative_folder_assets_subFolder_assets>,
};

export type HomeDetails_reactNative_folder_assets = {
  __typename: "Asset",
  order: number,
  id: string,
  subFolder: ?HomeDetails_reactNative_folder_assets_subFolder,
};

export type HomeDetails_reactNative_folder = {
  __typename: "Folder",
  id: string,
  name: ?string,
  largeImage: ?string,
  smallImage: ?string,
  description: ?string,
  mobileBackgroundImageUri: ?string,
  tvReactNativeBackgroundImageUri: ?string,
  assets: Array<HomeDetails_reactNative_folder_assets>,
};

export type HomeDetails_reactNative = {
  __typename: "ReactNative",
  id: ?string,
  folder: HomeDetails_reactNative_folder,
};

export type HomeDetails = {
  reactNative: ?HomeDetails_reactNative
};

export type HomeDetailsVariables = {
  rnId: string
};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================