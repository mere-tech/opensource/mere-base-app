import React from 'react';
import { ScrollView, StatusBar, View, TouchableWithoutFeedback, Text } from 'react-native';
import { loader } from 'graphql.macro';
import { connect } from 'react-redux';
import DeviceInfo from 'react-native-device-info';
import Loading from '../../components/Loading';
import FolderGrid from '../../components/FolderGrid';
import HeaderLogo from '../../components/HeaderLogo';
import FeaturedItem from '../../components/FeaturedItem';
import AppBackground from "../../components/AppBackground";
import SideNav from '../../components/SideNav';

const query = loader('./home.graphql');
const SkipFirst = (folder) => 
  Object.assign({},
    folder,
    { assets: folder.assets.slice(1) }
  );

const styles = {
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#000000',
  },
};

const HomeViewScreen = ({ app }) => (
  <Loading
    query={query}
  >
    {({ data }) => (
      <View style={styles.container}>
        <SideNav />
        <ScrollView>
          <StatusBar hidden />
          <AppBackground backgroundImage={DeviceInfo.getDeviceType() === "Tv" ? data.reactNative.folder.assets[0].subFolder.tvReactNativeBackgroundImageUri : data.reactNative.folder.assets[0].subFolder.mobileBackgroundImageUri} />
          { DeviceInfo.getDeviceType() !== 'Tv' && (
            <TouchableWithoutFeedback 
              style={{ backgroundColor: 'transparent', height: 30, width: '100%' }}
              onPress={() => {
                alert(app.environment.version)
              }}
            >
              <View style={{ backgroundColor: 'transparent', height: 30, width: '100%'}} />
            </TouchableWithoutFeedback>
          )}
          <HeaderLogo />
          { app.environment.showFeatured && (<FeaturedItem asset={data.reactNative.folder.assets[0].subFolder.assets[0].video} />)}
          <FolderGrid showSubTitle={false} folder={ app.environment.showFeatured ? SkipFirst(data.reactNative.folder) : data.reactNative.folder} />
        </ScrollView>
      </View>
      )}
  </Loading>
);

const mapStateToProps = ({ app }: GlobalState) => ({ app });
export default connect(mapStateToProps)(HomeViewScreen)