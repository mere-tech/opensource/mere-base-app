// @flow
import { combineReducers } from 'redux';

// ## Generator Reducer Imports
import app from '../modules/AppState';
import auth from '../reducers/auth';
import type AppStateType from '../modules/AppState';

export type GlobalState = {
  app: AppStateType,
};

export default combineReducers({
  // ## Generator Reducers
  app,
  auth,
});
