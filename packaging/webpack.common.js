const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      path.resolve('./src'),
      path.resolve('./node_modules'),
      path.resolve('./src/lib'),
      path.resolve('./lib'),
      path.resolve('./src/assets'),
    ],
  },
  module: {
    rules: [
      { test: /\.js$/, loader: ['babel-loader', 'eslint-loader'], exclude: /node_modules/ },
      { test: /\.jsx$/, loader: ['babel-loader', 'eslint-loader'], exclude: /node_modules/ },
      {
        test: /\.(png|svg|jpg|gif|woff|woff2|ttf)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192
            }
          }
        ]
      },
      { 
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader'
      },
      {
        test: /\.css$/,
          loaders: [
            'style-loader',
            'css-loader'
          ]
      }
    ]
  },
  entry: {
    app: './src/index.js'
  },
  devServer: {
    historyApiFallback: true,
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
  ],
  output: {
    publicPath: '/',
    filename: 'app.bundle.js',
    path: path.resolve(__dirname, '../dist')
  }
};