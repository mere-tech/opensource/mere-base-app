const merge = require('webpack-merge');
const webpack = require('webpack');
const common = require('./webpack.common.js');

common.plugins = common.plugins.concat([]);

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: '../dist'
  }
});